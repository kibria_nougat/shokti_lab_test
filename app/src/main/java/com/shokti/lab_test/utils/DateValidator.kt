package com.shokti.lab_test.utils

interface DateValidator {
    fun isValid(dateStr: String): Boolean
    fun isDateValid(dateStr: String): Boolean
}