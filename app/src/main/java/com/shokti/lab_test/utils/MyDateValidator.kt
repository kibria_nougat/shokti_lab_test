package com.shokti.lab_test.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object MyDateValidator : DateValidator {

    private val dobDateFormatString: String by lazy {
        "dd-mm-yyyy"
    }

    private val dateFormatString: String by lazy {
        "dd-mm-yyyy"
    }

    override fun isValid(dateStr: String): Boolean {
        val sdf: DateFormat = SimpleDateFormat(dobDateFormatString)
        sdf.isLenient = false
        try {
            sdf.parse(dateStr)
        } catch (e: ParseException) {
            return false
        }
        return true
    }

    override fun isDateValid(dateStr: String): Boolean {
        val sdf: DateFormat = SimpleDateFormat(dateFormatString)
        sdf.isLenient = false
        try {
            sdf.parse(dateStr)
        } catch (e: ParseException) {
            return false
        }
        return true
    }

    fun isValidAge(
        dobStr: String
    ): Boolean {
        val today: Calendar = Calendar.getInstance()

        val sdf = SimpleDateFormat("dd-mm-yyyy")
        val dob = Calendar.getInstance()
        dob.time = sdf.parse(dobStr)

        val curYear: Int = today.get(Calendar.YEAR)
        val dobYear: Int = dob.get(Calendar.YEAR)

        var age = curYear - dobYear


        val curMonth: Int = today.get(Calendar.MONTH)
        val dobMonth: Int = dob.get(Calendar.MONTH)
        if (dobMonth > curMonth) { // this year can't be counted!
            age--
        } else if (dobMonth == curMonth) {// same month? check for day
            val curDay = today[Calendar.DAY_OF_MONTH]
            val dobDay = dob[Calendar.DAY_OF_MONTH]
            if (dobDay > curDay) { // this year can't be counted!
                age--
            }
        }

        return age in 18..100
    }

}