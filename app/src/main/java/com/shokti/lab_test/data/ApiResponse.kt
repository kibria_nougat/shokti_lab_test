package com.shokti.lab_test.data


class ApiResponse private constructor(
    val status: Status,
    val data: Any?,
    val message: String?,
    val error: Throwable?
) {

    companion object {
        fun loading(): ApiResponse {
            return ApiResponse(
                Status.LOADING,
                null,
                null,
                null
            )
        }

        fun success(
            data: Any?,
            message: String?
        ): ApiResponse {
            return ApiResponse(
                Status.SUCCESS,
                data,
                message,
                null
            )
        }


        fun error(
            error: Throwable?,
            message: String?
        ): ApiResponse {
            return ApiResponse(
                Status.ERROR,
                null,
                message,
                error
            )
        }
    }

}