package com.shokti.lab_test.data

enum class Status {
    SUCCESS,
    FAILED,
    ERROR,
    LOADING
}