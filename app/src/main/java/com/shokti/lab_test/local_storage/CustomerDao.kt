package com.shokti.lab_test.local_storage

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single


@Dao
interface CustomerDao {

    @Insert
    fun insertCustomers(tasks: Customers): Completable



    @Query("SELECT * FROM customers ORDER BY customer_id DESC LIMIT 5")
    fun getCustomers(): Single<List<Customers>>


}