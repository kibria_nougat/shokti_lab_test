package com.shokti.lab_test.local_storage

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Customers(
    @ColumnInfo(name = "customer_id")
    @PrimaryKey(autoGenerate = true) val customer_id: Int?,
    @ColumnInfo(name = "customer_name") var customer_name: String,
    @ColumnInfo(name = "customer_dob") var customer_dob: String?,
    @ColumnInfo(name = "customer_contact") var customer_contact: String,
    @ColumnInfo(name = "customer_email") var customer_email: String?
)