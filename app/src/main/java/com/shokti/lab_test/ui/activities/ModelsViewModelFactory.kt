package com.shokti.lab_test.ui.activities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shokti.lab_test.local_storage.AppDatabase

class ModelsViewModelFactory(private val appDatabase: AppDatabase)  : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(appDatabase) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}