package com.shokti.lab_test.ui.activities

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shokti.lab_test.data.ApiResponse
import com.shokti.lab_test.local_storage.AppDatabase
import com.shokti.lab_test.local_storage.Customers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(appDatabase: AppDatabase) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val mutableLiveData: MutableLiveData<ApiResponse> = MutableLiveData<ApiResponse>()
    private var appDatabase: AppDatabase = appDatabase

    fun addNewCustomers(customers: Customers) {
        compositeDisposable.add(
            appDatabase.customerDao()
                .insertCustomers(customers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { mutableLiveData.setValue(ApiResponse.loading()) }
                .subscribe({
                    mutableLiveData.value = ApiResponse.success(null, "New customer added!")
                    Log.d("AppDbTesting", "IsInserted First Time")
                }, { throwable ->
                    mutableLiveData.value = ApiResponse.error(throwable, "Error happened!")
                    Log.d("AppDbTesting", "IsInserted ${throwable.localizedMessage}")
                })
        )
    }


    fun getLatestCustomers() {
        compositeDisposable.add(
            appDatabase.customerDao()
                .getCustomers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { mutableLiveData.setValue(ApiResponse.loading()) }
                .subscribe({
                    mutableLiveData.value = ApiResponse.success(it, "Data loaded successfully!")
                    Log.d("AppDbTesting", "IsInserted First Time")
                }, { throwable ->
                    mutableLiveData.value = ApiResponse.error(throwable, "Error happened!")
                    Log.d("AppDbTesting", "IsInserted ${throwable.localizedMessage}")
                })
        )
    }


    fun getResponse(): MutableLiveData<ApiResponse> {
        return mutableLiveData
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}