package com.shokti.lab_test.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shokti.lab_test.R
import com.shokti.lab_test.local_storage.Customers
import kotlinx.android.synthetic.main.item_customer_layout.view.*


class CustomerListAdapter(private val dataList: List<Customers>) :
    RecyclerView.Adapter<CustomerListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_customer_layout, parent, false)
        )
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)
    }


    inner class MyViewHolder(inflatedView: View) : RecyclerView.ViewHolder(inflatedView) {
        private val view: View = inflatedView
        private val tvCustomerName = view.tvCustomerName
        private val tvCustomerID = view.tvCustomerID
        private val tvCustomerContact = view.tvCustomerContact
        private val tvCustomerDOB = view.tvCustomerDOB


        fun bind(data: Customers) {
            tvCustomerName.text = data.customer_name
            tvCustomerID.text = data.customer_id.toString()
            tvCustomerContact.text = data.customer_contact
            tvCustomerDOB.text = data.customer_dob
        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

}