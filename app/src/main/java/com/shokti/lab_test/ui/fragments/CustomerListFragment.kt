package com.shokti.lab_test.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.shokti.lab_test.R
import com.shokti.lab_test.data.ApiResponse
import com.shokti.lab_test.data.Status
import com.shokti.lab_test.local_storage.AppDatabase
import com.shokti.lab_test.local_storage.Customers
import com.shokti.lab_test.ui.activities.MainViewModel
import com.shokti.lab_test.ui.activities.ModelsViewModelFactory
import com.shokti.lab_test.ui.adapters.CustomerListAdapter
import kotlinx.android.synthetic.main.fragment_customer_list.view.*


class CustomerListFragment : Fragment() {

    private lateinit var mAdapter: CustomerListAdapter
    private lateinit var viewModel: MainViewModel
    private var progressCircular: ContentLoadingProgressBar? = null
    private var customers: MutableList<Customers> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val parentView = inflater.inflate(R.layout.fragment_customer_list, container, false)
        progressCircular = parentView.progressCircular
        setAdapter(parentView)
        initViewModel()

        return parentView
    }


    private fun initViewModel() {
        var appDatabase = AppDatabase.invoke(requireContext())
        viewModel = ViewModelProvider(this, ModelsViewModelFactory(appDatabase))
            .get(MainViewModel::class.java)
        viewModel.getResponse().observe(requireActivity(), androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getLatestCustomers()
    }


    private fun consumeResponse(apiResponse: ApiResponse) {
        Log.d("ApiTesting", "consumeResponse")
        when (apiResponse.status) {
            Status.LOADING -> {
                showProgressBar()
            }
            Status.SUCCESS -> {
                hideProgressBar()
                val list = apiResponse.data as List<Customers>
                resetAdapter(list)
            }

            Status.FAILED, Status.ERROR -> {
                Toast.makeText(context, apiResponse.message, Toast.LENGTH_SHORT).show()
                hideProgressBar()
            }
        }
    }

    private fun setAdapter(view: View) {
        mAdapter = CustomerListAdapter(customers)
        view.rvCustomerList.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            adapter = mAdapter
        }
    }


    private fun resetAdapter(list: List<Customers>) {
        customers.clear()
        customers.addAll(list)
        mAdapter.notifyDataSetChanged()
    }


    private fun showProgressBar() {
        progressCircular?.visibility = View.VISIBLE
    }


    private fun hideProgressBar() {
        progressCircular?.visibility = View.GONE
    }

}