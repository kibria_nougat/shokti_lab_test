package com.shokti.lab_test.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.cardview.widget.CardView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.shokti.lab_test.R
import com.shokti.lab_test.data.ApiResponse
import com.shokti.lab_test.data.Status
import com.shokti.lab_test.local_storage.AppDatabase
import com.shokti.lab_test.local_storage.Customers
import com.shokti.lab_test.ui.activities.MainViewModel
import com.shokti.lab_test.ui.activities.ModelsViewModelFactory
import com.shokti.lab_test.utils.MyDateValidator
import kotlinx.android.synthetic.main.fragment_customer_reg.view.*
import java.util.*
import java.util.regex.Pattern


class CustomerRegFragment : Fragment(), View.OnClickListener {


    private lateinit var tilCustomerName: TextInputLayout
    private lateinit var etCustomerName: AppCompatEditText

    private lateinit var tilDob: TextInputLayout
    private lateinit var etDob: AppCompatEditText

    private lateinit var tilContactNo: TextInputLayout
    private lateinit var etContactNo: AppCompatEditText

    private lateinit var tilEmailAddress: TextInputLayout
    private lateinit var etEmailAddress: AppCompatEditText

    private lateinit var cvCustomerRegistration: CardView
    private lateinit var progressCircular: ContentLoadingProgressBar
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val parentView = inflater.inflate(R.layout.fragment_customer_reg, container, false)

        initView(parentView)
        initListeners()
        initViewModel()

        return parentView
    }


    private fun initView(parentView: View) {
        tilCustomerName = parentView.tilCustomerName
        etCustomerName = parentView.etCustomerName

        tilDob = parentView.tilDob
        etDob = parentView.etDob

        tilContactNo = parentView.tilContactNo
        etContactNo = parentView.etContactNo

        tilEmailAddress = parentView.tilEmailAddress
        etEmailAddress = parentView.etEmailAddress

        cvCustomerRegistration = parentView.cvCustomerRegistration
        progressCircular = parentView.progressCircular

        setHasOptionsMenu(true)
    }


    private fun initListeners() {
        cvCustomerRegistration.setOnClickListener(this)
        DateInputMask(etDob)
    }


    private fun initViewModel() {
        var appDatabase = AppDatabase.invoke(requireContext())
        viewModel = ViewModelProvider(this, ModelsViewModelFactory(appDatabase))
            .get(MainViewModel::class.java)
        viewModel.getResponse().observe(requireActivity(), androidx.lifecycle.Observer {
            consumeResponse(it)
        })
    }


    private fun consumeResponse(apiResponse: ApiResponse) {
        Log.d("ApiTesting", "consumeResponse")
        when (apiResponse.status) {
            Status.LOADING -> {
                showProgressBar()
            }
            Status.SUCCESS -> {
                hideProgressBar()
                Toast.makeText(context, apiResponse.message, Toast.LENGTH_SHORT).show()
            }

            Status.FAILED, Status.ERROR -> {
                hideProgressBar()
                Toast.makeText(context, apiResponse.message, Toast.LENGTH_SHORT).show()
            }
        }
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.cvCustomerRegistration -> {

                if (validateInputs()) {
                    val customerName = etCustomerName.text?.trim().toString()
                    val customerDob = etDob.text?.trim().toString()
                    val customerContactNo = etContactNo.text?.trim().toString()
                    val customerEmailAddress = etEmailAddress.text?.trim().toString()

                    val customer = Customers(
                        null,
                        customerName,
                        customerDob,
                        customerContactNo,
                        customerEmailAddress
                    )
                    viewModel.addNewCustomers(customer)
                }
            }
        }
    }


    private fun validateInputs(): Boolean {
        val customerName = etCustomerName.text?.trim().toString()
        val customerDob = etDob.text?.trim().toString()
        val customerContactNo = etContactNo.text?.trim().toString()
        val customerEmailAddress = etEmailAddress.text?.trim().toString()

        if (customerName.isNullOrEmpty()) {
            tilCustomerName.error = getString(R.string.required_field)
            return false
        }
        else if(doesNameContainDigit(customerName)){
            tilCustomerName.error = getString(R.string.name_can_not_have_digit)
            return false
        }
        else {
            tilCustomerName.error = null
        }

        if (customerDob.isNullOrEmpty()) {
            tilDob.error = getString(R.string.required_field)
            return false
        } else if (!MyDateValidator.isValid(customerDob)) {
            tilDob.error = getString(R.string.date_not_valid)
            return false
        }
        else if (!MyDateValidator.isValidAge(customerDob)) {
            tilDob.error = getString(R.string.age_validation)
            return false
        }
        else{
            tilDob.error = null
        }

        if (customerContactNo.isNullOrEmpty()) {
            tilContactNo.error = getString(R.string.required_field)
            return false
        }
        else if(customerContactNo.length<11){
            tilContactNo.error = getString(R.string.contact_no_not_valid)
            return false
        }
        else {
            tilContactNo.error = null
        }

        if (customerEmailAddress.isNullOrEmpty()) {
            tilEmailAddress.error = getString(R.string.required_field)
            return false
        }
        else if(!isEmailValid(customerEmailAddress)){
            tilEmailAddress.error = getString(R.string.invalid_email)
            return false
        }else {
            tilEmailAddress.error = null
        }

        return true
    }


    private fun showProgressBar() {
        progressCircular?.visibility = View.VISIBLE
    }


    private fun hideProgressBar() {
        progressCircular?.visibility = View.GONE
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_customers -> {
                val navController = requireActivity().findNavController(R.id.nav_host_fragment)
                navController.navigate(R.id.action_customer_reg_fragment_to_customer_fragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    internal class DateInputMask(input: EditText) : TextWatcher {
        private var current = ""
        private val ddmmyyyy = "DDMMYYYY"
        private val cal: Calendar = Calendar.getInstance()
        private val input: EditText = input

        init {
            this.input.addTextChangedListener(this)
        }

        override fun beforeTextChanged(
            s: CharSequence?,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
            if (s.toString() == current) {
                return
            }
            var clean = s.toString().replace("[^\\d.]|\\.".toRegex(), "")
            val cleanC = current.replace("[^\\d.]|\\.".toRegex(), "")
            val cl = clean.length
            var sel = cl
            var i = 2
            while (i <= cl && i < 6) {
                sel++
                i += 2
            }
            //Fix for pressing delete next to a forward slash
            if (clean == cleanC) sel--
            if (clean.length < 8) {
                clean += ddmmyyyy.substring(clean.length)
            } else {
                //This part makes sure that when we finish entering numbers
                //the date is correct, fixing it otherwise
                var day = clean.substring(0, 2).toInt()
                var mon = clean.substring(2, 4).toInt()
                var year = clean.substring(4, 8).toInt()
                mon = if (mon < 1) 1 else if (mon > 12) 12 else mon
                cal.set(Calendar.MONTH, mon - 1)
                year = if (year < 1900) 1900 else if (year > 2100) 2100 else year
                cal.set(Calendar.YEAR, year)
                // ^ first set year for the line below to work correctly
                //with leap years - otherwise, date e.g. 29/02/2012
                //would be automatically corrected to 28/02/2012
                day =
                    if (day > cal.getActualMaximum(Calendar.DATE)) cal.getActualMaximum(Calendar.DATE) else day
                clean = String.format("%02d%02d%02d", day, mon, year)
            }
            clean = String.format(
                "%s-%s-%s", clean.substring(0, 2),
                clean.substring(2, 4),
                clean.substring(4, 8)
            )
            sel = if (sel < 0) 0 else sel
            current = clean
            input.setText(current)
            input.setSelection(if (sel < current.length) sel else current.length)
        }

        override fun afterTextChanged(s: Editable?) {}

    }



    private fun isEmailValid(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    private fun doesNameContainDigit(input: String): Boolean{
        val pattern = Pattern.compile(".*\\d+.*")
        return pattern.matcher(input).matches()
    }

}